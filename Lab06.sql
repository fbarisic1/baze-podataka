--ZADATAK 1
select 
* 
from 
    common.naselja n, 
    common.zupanije z
Where
    n.idzupanija = z.id
 
--ZADATAK 2   
select
    n.mjesto,
    n.opcina as op�ina,
    n.postanskibroj as po�ta,
    z.zupanija as �upanija
    
from 
    common.naselja n, 
    common.zupanije z
Where
    n.idzupanija = z.id

--ZADATAK 3
SELECT TO_CHAR(SYSDATE, 'DD.FMMonth YYYY.') AS danasnji_datum FROM DUAL;

--ZADATAK 4
SELECT UPPER(SUBSTR(ime, 1, 1) || SUBSTR(prezime, 1, 2)) AS Inicijali,
       ime AS Ime,
       prezime AS Prezime
FROM common.korisnici;

--ZADATAK 5
SELECT LOWER(SUBSTR(ime, 1, 1) || prezime) AS Username,
       ime AS Ime,
       prezime AS Prezime
FROM common.korisnici;

--ZADATAK 6
SELECT 
    CONCAT(ime, ' ' || prezime) AS naziv,
    lpad(pozmob, 3, 0)  || '/' || mobbro as poz,
    CASE spol
        WHEN  0 THEN 'mu�ko'
        WHEN  1 THEN '�ensko'
    END AS spol
FROM common.kontakti kon inner join 
     common.korisnici kor on
     kor.id = kon.idkorisnika
      
--ZADATAK 7 
SELECT 
    pozmob
FROM common.kontakti

--ZADATAK 8
SELECT 
    CONCAT(ime, ' ' || prezime) AS naziv,
    lpad(pozmob, 3, 0)  || '/' || mobbro as poz,
    
    CASE spol
        WHEN  0 THEN 'mu�ko'
        WHEN  1 THEN '�ensko'
    END AS spol,
    
    CASE pozmob
        WHEN 95 THEN 'TELE2'
        WHEN 92  THEN 'TOMATO'
        WHEN 91 THEN 'A1'
    END AS operater
    
FROM 
    common.kontakti kon,
    common.korisnici kor
WHERE
     kor.id = kon.idkorisnika

--ZADATAK 9
SELECT 
    CONCAT(ime, ' ' || prezime) AS naziv,
    lpad(pozmob, 3, 0)  || '/' || mobbro as poz,
    
    CASE spol
        WHEN  0 THEN 'mu�ko'
        WHEN  1 THEN '�ensko'
    END AS spol,
    
    CASE pozmob
        WHEN 95 THEN 'TELE2'
        WHEN 92  THEN 'TOMATO'
        WHEN 91 THEN 'A1'
    END AS operater
    
FROM 
    common.kontakti kon,
    common.korisnici kor
WHERE
    kor.id = kon.idkorisnika
    AND spol = 0
    AND pozmob = 95;

--ZADATAK 10
SELECT
    kor.ime,
    kor.prezime,
    nas.mjesto,
    nas.opcina
FROM
    common.korisnici kor,
    common.kontakti kon,
    common.naselja nas
WHERE
    kor.id = kon.idkorisnika
    AND kon.idnaselje = nas.idnaselje
    AND nas.idzupanije = 14;










