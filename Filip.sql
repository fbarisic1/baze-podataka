//ZADATAK 1
select * from common.naselja where idzupanija = 14

//ZADATAK 2
select * from common.naselja where idzupanija = 14 and mjesto like 'V%'

//ZADATAK 3
select * from common.naselja where upper(mjesto) like '%VUK%'

//ZADATAK 4
select * from common.naselja where mjesto like '%i�i'

//ZADATAK 5
select
    z.zupanija,
    count(1)
from
   common.zupanije z,
   common.naselja n
where 
    n.idzupanija = z.id and
    mjesto like '%i�i'
group by
    z.zupanija
order by
    count(2)

select * from common.naselja
select * from common.zupanije
select * from common.korisnici

//ZADATAK 6
select count(*) from common.naselja

//ZADATAK 7
select count(*) from common.naselja where upper(mjesto) like 'N%'

//ZADATAK 8
select count(*) from common.naselja where upper(mjesto) like 'N______'

//ZADATAK 9
select opcina,
count(1)

from
    common.naselja
group by 
    opcina
order by
    count(1)
    desc

//ZADATAK 10
select
    z.zupanija,
    count(1)
from
   common.zupanije z,
   common.naselja n
where 
    n.idzupanija = z.id 
group by
    z.zupanija
order by
    count(2)

select * from common.naselja
select * from common.zupanije
select * from common.korisnici

//ZADATAK 11
CREATE TABLE NASELJA (
	id NUMBER(4, 0) NOT NULL,
	mjesto VARCHAR2(30) NOT NULL,
	Postanski_Broj NUMBER(6, 0) NOT NULL,
	opcina VARCHAR2(30) NOT NULL,
	IDzupanija INT NOT NULL,
	constraint NASELJA_PK PRIMARY KEY (id));


/
CREATE TABLE ZUPANIJE (
	id NUMBER(2, 0) NOT NULL,
	Zupanija VARCHAR2(30) NOT NULL,
	constraint ZUPANIJE_PK PRIMARY KEY (id));

CREATE sequence ZUPANIJE_ID_SEQ;

CREATE trigger BI_ZUPANIJE_ID
  before insert on ZUPANIJE
  for each row
begin
  select ZUPANIJE_ID_SEQ.nextval into :NEW.id from dual;
end;

/
ALTER TABLE NASELJA ADD CONSTRAINT NASELJA_fk0 FOREIGN KEY (IDzupanija) REFERENCES ZUPANIJE(id);

//ZADATAK 12
//zupanija je parent, a naselja smijemo izbrosati

//ZADATAK 13
select 
    * 
from 
    common.korisnici, 
    common.kontakti


//ZADATAK 14
select count(*) from common.korisnici where mreza like '097'
