drop table students;
drop table buses;

CREATE TABLE buses
( id number(6) NOT NULL PRIMARY KEY,
  reg_oznaka varchar2(10) NOT NULL,
  cijena number(5,2) NOT NULL,
  CONSTRAINT UQ_reg_oznaka UNIQUE (reg_oznaka)
);


CREATE TABLE students
( id number(6) NOT NULL PRIMARY KEY,
  JMBAG number(8) NOT NULL,
  autobus_id number(6),
  ime varchar2(50) NOT NULL,
  prezime varchar2(50) NOT NULL,
  e_mail  varchar2(50) NOT NULL,
  CONSTRAINT UQ_JMBAG UNIQUE (JMBAG),
  CONSTRAINT fk_autobus FOREIGN KEY(autobus_id)
  references buses(id)
);


insert into buses (ID, reg_oznaka, cijena) values (1, 'BJ 345 GH', 159.99);
insert into buses (ID, reg_oznaka, cijena) values (2, 'BJ 472 CD', 199.99);
insert into buses (ID, reg_oznaka, cijena) values (3, 'BJ 043 GB', 359.99);
insert into buses (ID, reg_oznaka, cijena) values (4, 'BJ 654 MK', 300.00);
insert into buses (ID, reg_oznaka, cijena) values (5, 'BJ 332 TS', 145.45);

insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (1, 36985478, 1, 'Mia', 'Horvat', 'mhorvat@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (2, 36985479, 1, 'Lucija', 'Kova�evi�', 'lkovacevic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (3, 36985480, 1, 'Ema', 'Babi�', 'ebabic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (4, 36985481, 1, 'Ana', 'Mari�', 'amaric@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (5, 36985482, 1, 'Petra', 'Juri�', 'pjuric@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (6, 36985483, 2, 'Lana', 'Novak', 'lnovak@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (7, 36985484, null, 'Dora', 'Kova�i�', 'dkovacic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (8, 36985485, 2, 'Marta', 'Vukovi�', 'mvukovic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (9, 36985486, 2, 'Luka', 'Kne�evi�', 'lknezevuc@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (10, 36985487, 2, 'Marko', 'Markovi�', 'mmarkovic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (11, 36985488, null, 'Jakov', 'Petrovi�', 'jpetrovic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (12, 36985489, null, 'Ivan', 'Mati�', 'imatic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (13, 36985490, 3, 'Petar', 'Tomi�', 'ptomic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (14, 36985491, 3, 'Matej', 'Kova�', 'mkovac@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (15, 36985492, 3, 'Filip', 'Pavlovi�', 'fpavlovic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (16, 36985493, null, 'Mario', 'Peri�', 'mperic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (17, 36985494, null, 'Anita', 'Jurkovi�', 'ajurkovic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (18, 36985495, null, 'Kazimir', 'Geli�', 'kgelic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (19, 36985496, null, 'Borna', '�piranec', 'spiranec@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (20, 36985497, null, 'Vesna', 'Molnar', 'vmolnar@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (21, 36985498, 2, 'Marija', 'Horvat', 'mhorvat1@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (22, 36985499, 2, 'Miroslav', 'Horvat', 'mhorvat2@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (23, 36985500, 3, 'Berislav', 'Mari�', 'bmaric@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (24, 36985501, 1, 'Jakov', 'Kova�i�', 'jkovacic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (25, 36985502, 1, 'Filip', 'Kova�i�', 'fkovacic@vub.hr');
insert into students (ID, JMBAG, autobus_id, ime, prezime, e_mail) values (26, 36985503, 1, 'Filip', 'Novak', 'fnovak@vub.hr');
commit;

--ZADATAK 1
SELECT * FROM buses;

--ZADATAK 2
SELECT AVG(cijena) AS srednja_cijena_autobusa
    FROM buses;

--ZADATAK 3
SELECT MAX(cijena) AS srednja_cijena_autobusa
    FROM buses;

--ZADATAK 4
SELECT MIN(cijena) AS srednja_cijena_autobusa
    FROM buses;

--ZADATAK 5
SELECT * FROM students
    WHERE autobus_id IS NULL;
    
--ZADATAK 6
SELECT COUNT(*) AS broj_studenata_ne_putuje
    FROM students
        WHERE students.autobus_id IS NULL;

--ZADATAK 7
SELECT * FROM students
    INNER JOIN buses ON students.autobus_id = buses.id
    WHERE students.autobus_id IS NOT NULL;

--ZADATAK 8
SELECT COUNT(*) AS broj_studenata
    FROM students
    INNER JOIN buses ON students.autobus_id = buses.id
    WHERE buses.reg_oznaka = 'BJ 345 GH';

--ZADATAK 9
SELECT * FROM students
    RIGHT JOIN buses ON students.autobus_id = buses.id;
    
--ZADATAK 10
SELECT * FROM students
    FULL JOIN buses ON students.autobus_id = buses.id;
    
--ZADATAK 11
SELECT * FROM buses 
    FULL JOIN students ON buses.id = students.autobus_id;
    
--ZADATAK 12
SELECT students.prezime, COUNT(*) AS broj_studenta
    FROM students
    INNER JOIN buses ON students.autobus_id = buses.id
    GROUP BY students.prezime
    ORDER BY COUNT(*) desc;

--ZADATAK 13
SELECT students.ime, students.prezime
    FROM students
    INNER JOIN buses ON students.autobus_id = buses.id
    ORDER BY students.prezime;
    
--ZADATAK 14
SELECT students.ime, students.prezime
    FROM students
    INNER JOIN buses ON students.autobus_id = buses.id
    ORDER BY students.ime, students.prezime;
    
--ZADATAK 15
SELECT students.ime, students.prezime
    FROM students
    INNER JOIN buses ON students.autobus_id = buses.id
    WHERE students.autobus_id IS NOT NULL
    ORDER BY students.ime, students.prezime;
    
--ZADATAK 16
SELECT buses.reg_oznaka, COUNT(students.id) AS ukupan_broj_studenata
    FROM buses
    LEFT JOIN students ON buses.id = students.autobus_id
    GROUP BY buses.reg_oznaka
    ORDER BY ukupan_broj_studenata DESC;

--ZADATAK 17
SELECT buses.reg_oznaka, COUNT(students.id) AS ukupan_broj_studenata, AVG(buses.cijena) AS jedinicna_cijena_prijevoza
    FROM buses
    LEFT JOIN students ON buses.id = students.autobus_id
    GROUP BY buses.reg_oznaka
    ORDER BY ukupan_broj_studenata DESC;

--ZADATAK 18
SELECT buses.reg_oznaka, COUNT(students.id) AS ukupan_broj_studenata, AVG(buses.cijena) AS jedinicna_cijena_prijevoza
    FROM buses
    LEFT JOIN students ON buses.id = students.autobus_id
    GROUP BY buses.reg_oznaka
    HAVING COUNT(students.id) > 4
    ORDER BY ukupan_broj_studenata DESC;

--ZADATAK 19
SELECT buses.reg_oznaka, COUNT(students.id) AS ukupan_broj_studenata, AVG(buses.cijena) AS jedinicna_cijena_prijevoza, COUNT(students.id) * AVG(buses.cijena) AS ukupna_cijena_autobusa
    FROM buses
    LEFT JOIN students ON buses.id = students.autobus_id
    GROUP BY buses.reg_oznaka
    HAVING COUNT(students.id) > 4
    ORDER BY ukupan_broj_studenata DESC;
    
--ZADATAK 20
SELECT buses.reg_oznaka, COUNT(students.id) AS ukupan_broj_studenata, AVG(buses.cijena) AS jedinicna_cijena_prijevoza, COUNT(students.id) * AVG(buses.cijena) AS ukupna_cijena_autobusa
    FROM buses
    LEFT JOIN students ON buses.id = students.autobus_id
    GROUP BY buses.reg_oznaka
    HAVING COUNT(students.id) * AVG(buses.cijena)  > 1200
    ORDER BY ukupan_broj_studenata DESC;