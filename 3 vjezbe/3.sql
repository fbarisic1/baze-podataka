drop table Upisi;
drop table Predmeti;
drop table Studenti;
drop table Nastavnici;





drop sequence STUDENTI_ID_SEQ;
drop sequence NASTAVNICI_ID_SEQ;
drop sequence PREDMETI_ID_SEQ;
drop sequence UPISI_ID_SEQ;



CREATE TABLE Studenti (
	id NUMBER(9, 0) NOT NULL,
	jmbag NUMBER(8, 0) UNIQUE NOT NULL,
	ime VARCHAR2(20) NOT NULL,
	prezime VARCHAR2(20) NOT NULL,
	email VARCHAR2(20) NOT NULL,
	password NUMBER(10, 0) UNIQUE NOT NULL,
	spol NUMBER(1, 0) NOT NULL,
	constraint STUDENTI_PK PRIMARY KEY (id),
    constraint ck_studenti_spol CHECK (spol in (0,1))
    );

CREATE sequence STUDENTI_ID_SEQ;

CREATE trigger BI_STUDENTI_ID
  before insert on Studenti
  for each row
begin
  select STUDENTI_ID_SEQ.nextval into :NEW.id from dual;
end;

/
CREATE TABLE Nastavnici (
	id NUMBER(9, 0) NOT NULL,
	ime VARCHAR2(20) NOT NULL,
	prezime VARCHAR2(30) NOT NULL,
	email VARCHAR2(30) UNIQUE NOT NULL,
	password NUMBER(20, 0) NOT NULL,
	spol NUMBER(1, 0) NOT NULL,
	constraint NASTAVNICI_PK PRIMARY KEY (id),
    constraint ck_nastavnici_spol CHECK (spol in (0,1))
    );

CREATE sequence NASTAVNICI_ID_SEQ;

CREATE trigger BI_NASTAVNICI_ID
  before insert on Nastavnici
  for each row
begin
  select NASTAVNICI_ID_SEQ.nextval into :NEW.id from dual;
end;

/
CREATE TABLE Predmeti (
	id NUMBER(9, 0) NOT NULL,
	idnastavnika NUMBER(9, 0) NOT NULL,
	sifra VARCHAR2(10) UNIQUE NOT NULL,
	naziv VARCHAR2(30) NOT NULL,
	ects NUMBER(1, 0) NOT NULL,
	semestar NUMBER(1, 0) NOT NULL,
	constraint PREDMETI_PK PRIMARY KEY (id),
    constraint ck_predmeti CHECK (semestar in (1,2,3,4,5,6))
    );

CREATE sequence PREDMETI_ID_SEQ;

CREATE trigger BI_PREDMETI_ID
  before insert on Predmeti
  for each row
begin
  select PREDMETI_ID_SEQ.nextval into :NEW.id from dual;
end;

/
CREATE TABLE Upisi (
	id NUMBER(9, 0) NOT NULL,
	IDpredmeta NUMBER(9, 0) NOT NULL,
	IDstudenta NUMBER(9, 0) NOT NULL,
	datum DATE NOT NULL,
	ocjena NUMBER(1, 0),
	constraint UPISI_PK PRIMARY KEY (id),
    constraint ck_ocjena CHECK (ocjena in (1,2,3,4,5))
    );

CREATE sequence UPISI_ID_SEQ;

CREATE trigger BI_UPISI_ID
  before insert on Upisi
  for each row
begin
  select UPISI_ID_SEQ.nextval into :NEW.id from dual;
end;

/


ALTER TABLE Predmeti ADD CONSTRAINT Predmeti_fk0 FOREIGN KEY (idnastavnika) REFERENCES Nastavnici(id);

ALTER TABLE Upisi ADD CONSTRAINT Upisi_fk0 FOREIGN KEY (IDpredmeta) REFERENCES Predmeti(id);
ALTER TABLE Upisi ADD CONSTRAINT Upisi_fk1 FOREIGN KEY (IDstudenta) REFERENCES Studenti(id);

INSERT ALL
INTO nastavnici (ime, prezime, email, password, spol) VALUES ('Pero', 'Peric', 'pperic@email.com', 'lozinka1', 1)
INTO nastavnici (ime, prezime, email, password, spol) VALUES ('Josip', 'Jopic', 'jjopic@email.com', 'lozinka2', 1)
INTO nastavnici (ime, prezime, email, password, spol) VALUES ('Zdenka', 'Zaba', 'zzaba@email.com', 'lozinka3', 0)
SELECT * FROM dual;


INSERT ALL
INTO studenti (jmbag, ime, prezime, email, password, spol) VALUES ('18923791', 'Ante', 'Prezime1', 'nastavnik1@email.com', 'lozinka1', 1)
INTO studenti (jmbag, ime, prezime, email, password, spol) VALUES ('81273981', 'Filip', 'Prezime1', 'nastavnik1@email.com', 'lozinka1', 1)
INTO studenti (jmbag, ime, prezime, email, password, spol) VALUES ('18237198', 'David', 'Prezime1', 'nastavnik1@email.com', 'lozinka1', 1)
INTO studenti (jmbag, ime, prezime, email, password, spol) VALUES ('83179381', 'Dario', 'Prezime1', 'nastavnik1@email.com', 'lozinka1', 1)
INTO studenti (jmbag, ime, prezime, email, password, spol) VALUES ('28931793', 'Marko', 'Prezime1', 'nastavnik1@email.com', 'lozinka1', 1)
INTO studenti (jmbag, ime, prezime, email, password, spol) VALUES ('18237918', 'Matijas', 'Prezime1', 'nastavnik1@email.com', 'lozinka1', 1)
INTO studenti (jmbag, ime, prezime, email, password, spol) VALUES ('19823718', 'Mislav', 'Prezime1', 'nastavnik1@email.com', 'lozinka1', 1)
INTO studenti (jmbag, ime, prezime, email, password, spol) VALUES ('18923719', 'Karlo', 'Prezime1', 'nastavnik1@email.com', 'lozinka1', 1)
INTO studenti (jmbag, ime, prezime, email, password, spol) VALUES ('12371823', 'Zoran', 'Prezime1', 'nastavnik1@email.com', 'lozinka1', 1)
INTO studenti (jmbag, ime, prezime, email, password, spol) VALUES ('19823187', 'Ivan', 'Prezime1', 'nastavnik1@email.com', 'lozinka1', 1)
SELECT * FROM dual;

INSERT ALL
INTO upisi (IDpredmeta, IDstudenata, datum, ocjena) VALUES ('Nastavnik1', 'Prezime1', 'nastavnik1@email.com', 'lozinka1', 1)
INTO upisi (IDpredmeta, IDstudenata, datum, ocjena) VALUES ('Nastavnik1', 'Prezime1', 'nastavnik1@email.com', 'lozinka1', 1)
INTO upisi (IDpredmeta, IDstudenata, datum, ocjena) VALUES ('Nastavnik1', 'Prezime1', 'nastavnik1@email.com', 'lozinka1', 1)
INTO upisi (IDpredmeta, IDstudenata, datum, ocjena) VALUES ('Nastavnik1', 'Prezime1', 'nastavnik1@email.com', 'lozinka1', 1)
SELECT * FROM dual;

INSERT ALL
INTO predmeti (idnastavnika, sifra, naziv, ects, semestar) VALUES ('Nastavnik1', 'Nastavnik1', 'Prezime1', 'nastavnik1@email.com', 'lozinka1')
INTO predmeti (idnastavnika, sifra, naziv, ects, semestar) VALUES ('Nastavnik1', 'Nastavnik1', 'Prezime1', 'nastavnik1@email.com', 'lozinka1')
INTO predmeti (idnastavnika, sifra, naziv, ects, semestar) VALUES ('Nastavnik1', 'Nastavnik1', 'Prezime1', 'nastavnik1@email.com', 'lozinka1')
SELECT * FROM dual;